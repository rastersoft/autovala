/*
 * Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
 *
 * This file is part of AutoVala
 *
 * AutoVala is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * AutoVala is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

using GLib;

namespace AutoVala {
	private class ElementBuildExternalProgram : ElementBase {
		public ElementBuildExternalProgram() {
			this._type   = ConfigType.BUILD_EXTERNAL_PROGRAM;
			this.command = "build_external_program";
		}

		public override void add_files() {
			this.file_list = {};
		}

		public override bool autoConfigure(string ? path = null) {
			return false;
		}

		public bool addBuildExternalProgram(string program, bool automatic, string ? condition, bool invertCondition) {
			if (this.findElement(ConfigType.BUILD_EXTERNAL_PROGRAM, null, program, program)) {
				return false;
			}
			return this.configureElement(null, program, program, automatic, condition, invertCondition);
		}

		public override bool configureLine(string line, bool automatic, string ? condition, bool invertCondition, int lineNumber, string[] ? comments) {
			if (false == line.has_prefix(this.command + ": ")) {
				var badCommand = line.split(": ")[0];
				ElementBase.globalData.addError(_("Invalid command %s after command %s (line %d)").printf(badCommand, this.command, lineNumber));
				return true;
			}
			var data = line.substring(2 + this.command.length).strip();
			this.comments = comments;
			return this.addBuildExternalProgram(data, automatic, condition, invertCondition);
		}
	}
}
