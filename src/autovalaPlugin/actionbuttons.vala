using Gtk;
using Gee;
using AutoVala;
//using GIO
namespace AutovalaPlugin {
	enum ButtonNames { NEW = 1, REFRESH = 2, UPDATE = 4, BUILD = 8, FULL_BUILD = 16, PO = 32 }

	/**
	 * Shows the upper action buttons that allows to create a new project,
	 * update the CMake files, translations, and so on.
	 * This widget needs a ProjectView widget in order to work.
	 */
	public class ActionButtons : Gtk.Box {
		private Gtk.Button new_project;
		private Gtk.Button refresh_project;
		private Gtk.Button update_project;
		private Gtk.Button update_translations;
		private Gtk.Button build_project;
		private Gtk.Button full_build_project;

		/**
		 * This signal is emited when a new project has been created
		 * @param path The full path to the default source file
		 */
		public signal void open_file(string path);

		public signal void action_new_project();
		public signal void action_refresh_project();
		public signal void action_update_project();
		public signal void action_update_gettext();
		public signal void action_build();
		public signal void action_full_build();

		private Gtk.Image pixbuf_build;
		private Gtk.Image pixbuf_full_build;
		private Gtk.Image pixbuf_refresh;
		private Gtk.Image pixbuf_refresh_langs;
		private Gtk.Image pixbuf_update;

		private Gtk.Image load_pixbuf_from_resource(string resource) {
			var pixbuf = new Gtk.Image.from_resource(resource).get_pixbuf();
			int w, h;
			if (pixbuf.width > pixbuf.height) {
				w = 24;
				h = (pixbuf.height * 24 / pixbuf.width);
			} else {
				h = 24;
				w = (pixbuf.width * 24 / pixbuf.height);
			}
			var new_pixbuf = pixbuf.scale_simple(w,h,Gdk.InterpType.BILINEAR);
			return new Gtk.Image.from_pixbuf(new_pixbuf);
		}

		public ActionButtons() {
			int sep_margin = 2;

			this.pixbuf_build = this.load_pixbuf_from_resource("/com/rastersoft/autovala/pixmaps/build.svg");
			this.pixbuf_full_build = this.load_pixbuf_from_resource("/com/rastersoft/autovala/pixmaps/full_build.svg");
			this.pixbuf_refresh = this.load_pixbuf_from_resource("/com/rastersoft/autovala/pixmaps/refresh.svg");
			this.pixbuf_refresh_langs = this.load_pixbuf_from_resource("/com/rastersoft/autovala/pixmaps/refresh_langs.svg");
			this.pixbuf_update = this.load_pixbuf_from_resource("/com/rastersoft/autovala/pixmaps/update.svg");

			this.orientation = Gtk.Orientation.HORIZONTAL;

			this.new_project = new Gtk.Button.from_icon_name("document-new", Gtk.IconSize.LARGE_TOOLBAR);
			this.new_project.tooltip_text = _("Creates a new Autovala project");
			this.pack_start(this.new_project, false, false);

			var sep1 = new Gtk.Separator(Gtk.Orientation.VERTICAL);
			sep1.margin_start = sep_margin;
			sep1.margin_end   = sep_margin;

			this.pack_start(sep1, false, false);

			this.refresh_project = new Gtk.Button();
			this.refresh_project.set_image(this.pixbuf_refresh);
			this.refresh_project.tooltip_text = _("Refreshes the Autovala project");
			this.pack_start(this.refresh_project, false, false);

			this.update_project = new Gtk.Button();
			this.update_project.set_image(this.pixbuf_update);
			this.update_project.tooltip_text = _("Updates the project and rebuilds the CMake files");
			this.pack_start(this.update_project, false, false);

			this.build_project = new Gtk.Button();
			this.build_project.set_image(this.pixbuf_build);
			this.build_project.tooltip_text = _("Builds the Autovala project");
			this.pack_start(this.build_project, false, false);

			this.full_build_project = new Gtk.Button();
			this.full_build_project.set_image(this.pixbuf_full_build);
			this.full_build_project.tooltip_text = _("Builds the Autovala project");
			this.pack_start(this.full_build_project, false, false);

			sep1 = new Gtk.Separator(Gtk.Orientation.VERTICAL);
			sep1.margin_start = sep_margin;
			sep1.margin_end   = sep_margin;

			this.pack_start(sep1, false, false);

			this.update_translations = new Gtk.Button();
			this.update_translations.set_image(this.pixbuf_refresh_langs);
			this.update_translations.tooltip_text = _("Updates the language translation files");
			this.pack_start(this.update_translations, false, false);

			this.new_project.clicked.connect(() => {
				this.action_new_project();
			});

			this.refresh_project.clicked.connect(() => {
				this.action_refresh_project();
			});

			this.update_project.clicked.connect(() => {
				this.action_update_project();
			});

			this.update_translations.clicked.connect(() => {
				this.action_update_gettext();
			});

			this.build_project.clicked.connect(() => {
				this.action_build();
			});

			this.full_build_project.clicked.connect(() => {
				this.action_full_build();
			});

			this.show_all();
		}

		public void update_buttons(uint32 mode) {
			this.new_project.sensitive         = ((mode & ButtonNames.NEW) == 0) ? false : true;
			this.refresh_project.sensitive     = ((mode & ButtonNames.REFRESH) == 0) ? false : true;
			this.update_project.sensitive      = ((mode & ButtonNames.UPDATE) == 0) ? false : true;
			this.update_translations.sensitive = ((mode & ButtonNames.PO) == 0) ? false : true;
			this.build_project.sensitive       = ((mode & ButtonNames.BUILD) == 0) ? false : true;
			this.full_build_project.sensitive  = ((mode & ButtonNames.FULL_BUILD) == 0) ? false : true;
		}
	}
}
