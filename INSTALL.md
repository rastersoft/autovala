# HOW TO INSTALL

To compile and install it, just open a terminal, and go
to the directory where you downloaded the git repository.
Now, if you want to use Meson, type:

    mkdir meson
    cd meson
    meson
    ninja
    sudo ninja install
    sudo ldconfig

But if you prefer to use CMake, then type:

    mkdir install
    cd install
    cmake ..
    make
    sudo make install
    sudo ldconfig

If you are using GTK 3.4 or previous, you must specify it
to avoid the plugins to use newer widgets, not supported
in old GTK versions:

    mkdir install
    cd install
    cmake .. -DGTK_OLD=on
    make
    sudo make install
    sudo ldconfig

The Gedit and Scratch plugins are compiled and installed
separately. Just follow the instructions available in each
folder.

The main dependencies list is:

    * atk
    * glib-2.0
    * gee
    * gtk-3
    * libxml2
    * vte-2.91
    * readline
    * pandoc
    * curl
    * cmake
    * vala
    * gettext
    * pkgconfig
    * intltool

Remember that you need CMake or Meson, Vala 0.30 or later, libgee
and gtk+. The repository includes a version of CMake for
Vala that includes some changes (not mine) to support
Valadoc. These changes still aren't available at the
oficial repository. If you want, you can get the oficial
files with:

    bzr checkout lp:~elementary-apps/+junk/cmake-modules

This will create a folder called *cmake-modules*. Inside
it will be a folder called *cmake*. Copy the later to the
Autovala project folder, overwriting the files inside.

Don't forget to keep the files FindValadoc.cmake and
Valadoc.cmake, needed to work with Valadoc.
